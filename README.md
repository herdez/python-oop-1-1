# Classes and Instances 


## Classes


You're about to create three modules with class definitions. You've already seen how to import packages such as `matplotlib`, etc., and you can organize your own code in a similar manner. For example, once you define the `Ride` class in a file **ride.py**, you can then import said code in `script.py` using:


```python
# Import only the Ride class

```

In addition to **ride.py** file, we also created another file **driver.py** that contains the `Driver` class. Import this class: 


```python
# Import only the Driver class

```

Create a `Passenger` class, another file **passenger.py** that contains it:  


```python
# Create Passenger class

```

## Instances

Now use these classes to create instances in `script.py`. First, make two instances of the `Passenger` class and assign them to the variables: 


```python
# Two instances of the Passenger class
gussi = None
serena = None

```

Next, make one instance of the `Driver` class and assign it to the variable, `taxi`.


```python
# One instance of the Driver class
taxi = None

```

Finally, make two instances of the `Ride` class and assign them to `ride_to_mexico` and `ride_home`. 


```python
# Two instances of the Ride class
ride_to_mexico = None
ride_home = None

```


All Tests must be `True`.

```python
"""driver code"""

print(isinstance(gussi, Passenger))
print(isinstance(serena, Passenger))
print(isinstance(taxi, Driver))
print(isinstance(ride_to_mexico, Ride))
print(isinstance(ride_home, Ride))

``` 





