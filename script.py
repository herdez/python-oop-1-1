"""
script.py: script module, where it can be instances created and evaluated.
"""

def main():
	pass


# this means that if this script is executed, then 
# main() will be executed
if __name__ == '__main__':
    main()